Prime Time Assets & Scene Collection  
https://gitlab.com/mealwhiles/prime-time

Assets that are not credited are made by me.  
If you find a mistake, please contact me!

Contact
--------
mealwhiles@mealwhiles.com

PGP Public Key  
https://keyserver.pgp.com/vkd/DownloadKey.event?keyid=0x6B7126B9690CF3A5  
E98F31A59E315126C46E3A6C6B7126B9690CF3A5  
Tutanota emails will be encrypted properly.

Graphics
--------

[0] "start", "pause", "end" scene  
"Inside Joke Drawings"  
ex pink-ravioli.png  
by PortalBot

Music
-----

{0} "end" scene  
"Super Mario Song Cringe Lofi Remix"  
file pan-dough-lofi.mp3  
by pan-dough

Other
-----

<0> "start", "pause", "end" scene  
"Liberation Sans"  
typeface asset  
by Steve Matteson  
licensed by Red Hat

<1> "start" scene
"Gotham Rounded"  
typeface asset

<1> "start", "logo", "10sec-left" scene  
"Super Mario Maker"  
typeface asset  
by Nintendo

Freedom-respecting Software
---------------------------

"Open Broadcaster Software"  
for recording  
https://obsproject.com

"GNU Image Manipulation Program"  
for image manipulation  
https://gimp.org

"Kdenlive"  
for video editing  
https://kdenlive.org

"Natron" 
for video compositing  
https://natrongithub.github.io

Proprietary Software
--------------------

Elgato Game Capture HD  
for capture card drivers  
in win-party-pack-panic.json  
https://elgato.com
